<?php
require 'PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

$name = addslashes(strip_tags($_POST['name']));
$message = addslashes(strip_tags($_POST['message']));
$email = addslashes(strip_tags($_POST['email']));

$htmlmessage = <<<MESSAGE
    <html>
        <head>
            <title>$subject</title>
        </head>
        
        <body>
            <p><strong>Nom: </strong>$name</p>
            <p><strong>Email: </strong>$email</p>
            <p><strong>Message: </strong>$message</p>
        </body>
    </html>
MESSAGE;

$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->Host = 'aspmx.l.google.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'myemail';                 // SMTP username
$mail->Password = 'mypassword';                           // SMTP password
$mail->SMTPSecure = 'SSL';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 25;                                    // TCP port to connect to

$mail->setFrom($email, $name);
$mail->addAddress('contact@ultimate-factory.fr', 'Ultimate Factory');     // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Formulaire de contact';
$mail->Body    = $htmlmessage;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
    $send = false;
} else {
    echo 'Message has been sent';
    $send = true;
}

echo json_encode($send);
